#include<sys/socket.h>
#include<sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <netinet/in.h>
int fd[2],quit,childPid;
void udp_client(int port_no)
{
	struct hostent *host;
	struct sockaddr_in server_addr;
	const char* localhost="127.0.0.1";

	host=(struct hostent *) gethostbyname(localhost); //check

	int newconnection=0;
	char receiver_data[10000],received_data[10000],fileName[1000];
	newconnection=socket(AF_INET,SOCK_DGRAM,0);
	if (newconnection == -1)
	{
		perror("Error in socket");
		fprintf(stderr,"Closing program");
		exit(1);
	}


	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port_no);
	server_addr.sin_addr =*((struct in_addr *)(host->h_addr));

	int length_of_address=sizeof(struct sockaddr);
	char temp[10000];
	char *myinput;


	int bytes;
	while(1==1)
	{
		gets(receiver_data);
point:
		strcpy(temp,receiver_data);
		//Separate the input data based on space
		myinput=strtok(temp," ");

		if( myinput== NULL)
			;
		else
		{
			if(!strcmp(myinput,"FileHash"))
			{
				myinput=strtok(NULL," ");
				if(myinput!=NULL)
				{
					if(!strcmp(myinput,"CheckAll"))
					{
						strcpy(receiver_data,"filecheck");
						sendto(newconnection,receiver_data,strlen(receiver_data),0,(struct sockaddr *)&server_addr,sizeof(struct sockaddr));

						for(;;)
						{
							bytes = recvfrom(newconnection,received_data,1024,0,(struct sockaddr *)&server_addr,&length_of_address);
							received_data[bytes]='\0';
							if(!strcmp("End Of File",received_data))
								break;
							fwrite(received_data,1,bytes,stdout);
							fflush(stdout);
						}
						printf(" filecheck complete\n");
						continue;
					}


					else if(!strcmp(myinput,"Verify"))
					{
						myinput=strtok(NULL," ");
						if(myinput!=NULL)
						{
							strcpy(receiver_data,"fileverify ");
							strcat(receiver_data,myinput);
							printf("Data to be sent \n");
							printf("%s ",receiver_data);
							sendto(newconnection,receiver_data,strlen(receiver_data),0,(struct sockaddr *)&server_addr, sizeof(struct sockaddr));
							for(;;)
							{
								bytes=recvfrom(newconnection,received_data,1024,0,
										(struct sockaddr *)&server_addr, &length_of_address);
								received_data[bytes] = '\0';
								if(!strcmp(received_data,"End Of File"))
									break;
								fwrite(received_data,1,bytes,stdout);
								fflush(stdout);
							}
							printf(" filecheck complete\n");
							continue;
						}
					}
				}
				else
				{
					printf("INVALID USE OF FileHash\n");
					puts("Usage : FileHash CheckAll or FileHash Verify");
				}

			}


			else if(!(strcmp(myinput,"IndexGet")))
			{
				myinput=strtok(NULL," ");
				if(myinput!=NULL)
				{
					if(!strcmp("ShortList",myinput))
					{
						myinput=strtok(NULL," ");
						if(myinput!=NULL)
						{
							strcpy(receiver_data,"indexshort ");
							strcat(receiver_data,myinput);
							myinput=strtok(NULL," ");
							if(myinput!=NULL)
							{
								int ll=strlen(receiver_data);
								receiver_data[ll]=' ';
								receiver_data[ll+1]='\0';

								strcat(receiver_data,myinput);

								sendto(newconnection,receiver_data,strlen(receiver_data),0,(struct sockaddr *)&server_addr,sizeof(struct sockaddr));
								for(;;)
								{
									bytes=recvfrom(newconnection,received_data,1024,0,(struct sockaddr *)&server_addr,&length_of_address);

									received_data[bytes]='\0';
									if(!strcmp("End Of File",received_data))
										break;
									fwrite(received_data,1,bytes,stdout);

								}

								continue;
							}
						}
					}

					else if(!strcmp("RegEx",myinput))
					{
						myinput=strtok(NULL," ");
						if(myinput!=NULL)
						{
							strcpy(receiver_data,"regex ");
							strcat(receiver_data,myinput);
							printf("Data sent : %s",receiver_data);
							sendto(newconnection,receiver_data,strlen(receiver_data),0,
									(struct sockaddr *)&server_addr,sizeof(struct sockaddr));

							for(;;)
							{
								bytes=recvfrom(newconnection,received_data,1024,0,
										(struct sockaddr *)&server_addr,&length_of_address);
								bytes--;

								received_data[++bytes]='\0';
								if(!strcmp("End Of File",received_data))
									break;
								printf("%s",received_data);
							}
							printf("\nData receiving over\n");
							continue;
						}

						else
							puts("Filename not received ");
					}


					else if(!strcmp(myinput,"LongList"))
					{
						strcpy(receiver_data,"indexlong ");
						sendto(newconnection,receiver_data,strlen(receiver_data),0,
								(struct sockaddr *)&server_addr,sizeof(server_addr));

						for(;;)
						{
							bytes=recvfrom(newconnection,received_data,1024,0,(struct sockaddr *)&server_addr,
									&length_of_address);
							received_data[bytes]='\0';

							if(!strcmp("End Of File",received_data))
								break;
							printf("%s",received_data);
						}
						continue;
					}
				}
			}

			else if(!strcmp(myinput,"FileUpload"))
			{
				char temp2[1000];
				myinput=strtok(NULL, " ");
				strcpy(temp2,myinput);
				strcpy(receiver_data,"upload ");
				strcpy(fileName,myinput);
				strcat(receiver_data,myinput);
				sendto(newconnection,receiver_data,1024,0,
						(struct sockaddr *)&server_addr,sizeof(struct sockaddr));
			}


			//
			else if(!strcmp("FileDownload",myinput))
			{
				myinput=strtok(NULL," ");
				if(myinput!=NULL)
				{
					strcpy(receiver_data,"download ");
					strcat(receiver_data,myinput);
					sendto(newconnection,receiver_data,1024,0,
							(struct sockaddr *)&server_addr,sizeof(struct sockaddr));
					printf("Download %s\n",myinput);


					strcat(myinput,"(1)");
					FILE *fp=fopen(myinput,"wb");
					int i;
					for(i=0;i<1000;++i)
					{
						received_data[i]=0;
					}

					for(;;)
					{
						bytes=recvfrom(newconnection,received_data,1024,0,
								(struct sockaddr*)&server_addr,&length_of_address);
						received_data[bytes]='\0';
						printf("Received %s and  %d  bytes\n",received_data,bytes);

						if(!(strcmp("End Of File",received_data)))
							break;
						fwrite(received_data, 1,bytes, fp);
					}
					fclose(fp);
					printf("File has been closed\n");
				}

				else printf("Download has got wrong arguments");

			}

			else if(!strcmp("Allow",myinput))
			{
				close(fd[1]);
				int bytes_no=read(fd[0],fileName,sizeof(fileName));
				strcpy(receiver_data,"FileDownload ");
				printf("filename: %s\n",fileName);
				strcat(receiver_data,fileName);
				goto point;
			}


			else if(!strcmp("Reject",myinput))

				printf("Closed the process\n");


			else
			{
				if((strcmp(receiver_data,"quit") == 0) || quit == 1)
				{

					sendto(newconnection,receiver_data,strlen(receiver_data),0,
							(struct sockaddr *)&server_addr, sizeof(struct sockaddr)) ;
					close(newconnection);
					exit(0);
					break;
				}

				sendto(newconnection,receiver_data,strlen(receiver_data),0,
						(struct sockaddr *)&server_addr, sizeof(struct sockaddr));
			}
		}
	}
	return ;
}
void tserver(int arg)
{
	int server=socket(AF_INET,SOCK_STREAM,0);
	if(server==-1)
	{
		printf("error creating socket\n");
		exit(1);
	}
	struct sockaddr_in sadd,cadd;
	sadd.sin_family=AF_INET;
	sadd.sin_port=htons(arg);
	sadd.sin_addr.s_addr=INADDR_ANY;
	int len=sizeof(struct sockaddr);
	bzero(&(sadd.sin_zero),8); 
	bind(server,(struct sockaddr*)&sadd,len);
	if (listen(server, 5) == -1) {
		perror("Listen");
		exit(1);
	}
	int connection = accept(server, (struct sockaddr *)&cadd,&len);
	printf("listening now on port %d ...\n",arg);
	char data[2000],reg[2001];
	while(1)
	{
		int temp=recv(connection,data,1024,0);
		data[temp]='\0';
		char*te,temp1[2000];
		strcpy(temp1,data);
		te=strtok(temp1," ");
		printf("%s\n",te);
		printf("The incoming command is %s. The whole is %s\n",te, data);
		if(strcmp(te,"quit")==0){
			printf("closing\n");
			quit=1;
			close(connection);
			exit(1);
		}else if(strcmp	(te,"filecheck")==0){
			system("find . -type f -exec sh -c 'printf \"%s %s \n\" \"$(ls -l --time-style=+%Y%m%d%H%M%S $1 )\" \"$(md5sum $1 | cut -d\" \" -f1)\"' '' '{}' '{}' \\; | awk '{print $7, $6, $8}' > temp");
			FILE*f=fopen("temp","r");
			char stream[2000]={0};
			while(1)
			{
				int b;
				if(feof(f))break;
				b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				write(connection,stream,1024);
			}
			system("rm temp");
			char end[]= "End Of File";
			strcpy(stream,end);
			write(connection,stream,1024);
		}else if(strcmp(te,"fileverify")==0)
		{
			strncpy(reg,data+strlen("fileverify "),100);
			char command[2000];
			strcpy(command,"openssl md5 ");strcat(command,reg);strcat(command," | cut -d\" \" -f2 > md5");system(command);strcpy(command,"date -r ./");strcat(command,reg);strcat(command," +%Y%m%d%H%M%S > date");system(command);strcpy(command,"paste md5 date > temp");
			system(command);
			strcpy(command,"rm md5 date");
			system(command);
			FILE*f=fopen("temp","r");
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				write(connection,stream,1024);
			}
			system("rm temp");
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			write(connection,stream,1024);
			fclose(f);
		}else if(strcmp(te,"indexlong")==0)
		{
			system("find . -printf '%p %TY/%Tm/%Td %TH:%Tm:%Tm size:%k \n' > temp");
			FILE*f=fopen("temp","r");
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				write(connection,stream,1024);
			}
			system("rm temp");
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			write(connection,stream,1024);
			fclose(f);
		}else if(strcmp(te,"indexshort")==0)
		{
			strncpy(reg,(char*)data+strlen("indexshort "),100);
			printf("Timestamps are is %s\n",reg);
			char *myinput = strtok (reg," ");
			char lower[2000],upper[2000];
			strcpy(lower,myinput);
			myinput = strtok (NULL, " ");
			strcpy(upper,myinput);
			system("ls -l --time-style=+%Y%m%d%H%M%S -t > temp");
			FILE* f1 = fopen( "temp", "r" );
			FILE* f2 = fopen( "out", "w" );
			char buff[20000],cpy[20000];
			int cntr=0;
			while ( fgets( buff, 1000, f1 ) != NULL )
			{
				if(cntr!=0 && cntr!=1)
				{
					strcpy(cpy,buff);
					int place=0;
					myinput = strtok (buff," ");
					while (myinput != NULL)
					{
						if(place==5)
						{
							printf("%s\n", myinput);
							if(strcmp(myinput,lower)>0 && strcmp(myinput,upper)<0)
							{
								fprintf(f2,"%s",cpy);
							}
						}
						place++;
						myinput = strtok (NULL," ");
					}
				}
				cntr++;
			}
			fclose(f1);
			fclose(f2);
			FILE *f = fopen("out","r");
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				write(connection,stream,1024);
			}
			system("rm temp");
			system("rm out");
			memset(stream,0,1024);
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			write(connection,stream,1024);
			fclose(f);
		}else if(strcmp(te,"regex")==0)
		{
			strncpy(reg,(char*)data+strlen("regex "),100);
			char comm[2000];
			strcpy(comm,"find . -name \"");
			strcat(comm,reg);
			strcat(comm,"\" > temp");
			system(comm);
			FILE *f = fopen("temp","r");
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				write(connection,stream,1024);
			}
			system("rm temp");
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			write(connection,stream,1024);
			fclose(f);
		}else if(strcmp(te,"upload")==0)
		{
			char name[100];
			strcpy(name,data+strlen("upload "));
			printf("File: %s\nUpload incoming... Type \"Allow\" or \"Reject\"\n",name);
			close(fd[0]);
			write(fd[1],name,strlen(name)+1);
		}else if(strcmp(te,"download")==0)
		{
			char filename[200];
			strcpy(filename,data+strlen("download "));
			FILE*f=fopen(filename,"rb");
			if(!f)
			{
				printf("Invalid file %s\n",filename);
				continue;
			}
			fseek(f, SEEK_SET, 0);
			printf("upload of %s is going to start\n",filename);
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';

				printf("server sending %s\n",stream);
				write(connection,stream,1024);
			}
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			write(connection,stream,1024);
			fclose(f);
			char packet[1024];
			char cmd[1000],temp[100];
			int stlen,ii;
			strcpy(cmd,"md5sum ");
			strcat(cmd,filename);
			strcat(cmd," | awk '{print $2, $1}'> t1");
			system(cmd);
			strcpy(cmd,"stat -c%s ");
			strcat(cmd,filename);
			strcat(cmd," > t2");
			system(cmd);
			strcpy(cmd,"paste t1 t2 | awk '{print $1, $2, $3}' > md5");
			system(cmd);
			strcpy(cmd,"rm t1 t2");
			system(cmd);

			FILE *f1= fopen("md5","r");
			fgets( packet, 100, f1 );
			stlen=strlen(packet);
			packet[stlen-1]='\0';
			printf("length of packet now is %d\n",(int)strlen(packet));
			printf("packet header : %s\n",packet);
			fclose(f1);
			strcpy(cmd,"rm md5");
			system(cmd);

			memset(stream,0,1024);
			strcpy(stream,packet);
			printf("server seding %s\n",stream);
			write(connection,stream,1024);
		}else{
			printf("%s\n" , data);
			fflush(stdout);
		}
	}
	close(server);
}
void userver(int arg)
{
	int server=socket(AF_INET,SOCK_DGRAM,0);
	if(server==-1)
	{
		printf("error creating socket\n");
		exit(1);
	}
	struct sockaddr_in sadd,cadd;
	int connection= socket(AF_INET,SOCK_DGRAM,0);
	sadd.sin_family=AF_INET;
	sadd.sin_port=htons(arg);
	sadd.sin_addr.s_addr=INADDR_ANY;
	int len=sizeof(struct sockaddr);
	bind(server,(struct sockaddr*)&sadd,len);
	printf("listening now on port %d ...\n",arg);
	char data[2000],reg[2001];
	while(1)
	{
		int temp=recvfrom(server,data,1024,0,(struct sockaddr*)&cadd,&len);
		data[temp]='\0';
		char*te,temp1[2000];
		strcpy(temp1,data);
		te=strtok(temp1," ");
		printf("%s\n",te);
		printf("The incoming command is %s. The whole is %s\n",te, data);
		if(strcmp(te,"quit")==0){
			printf("closing\n");
			quit=1;
			close(connection);
			exit(1);
		}else if(strcmp	(te,"filecheck")==0){
			system("find . -type f -exec sh -c 'printf \"%s %s \n\" \"$(ls -l --time-style=+%Y%m%d%H%M%S $1 )\" \"$(md5sum $1 | cut -d\" \" -f1)\"' '' '{}' '{}' \\; | awk '{print $7, $6, $8}' > temp");
			FILE*f=fopen("temp","r");
			char stream[2000]={0};
			while(1)
			{
				int b;
				if(feof(f))break;
				b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				sendto(server,stream,1024,0,(struct sockaddr*)&cadd,len);
			}
			system("rm temp");
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			sendto(server,stream,1024,0,(struct sockaddr*)&cadd,len);
		}else if(strcmp(te,"fileverify")==0)
		{
			strncpy(reg,data+strlen("fileverify "),100);
			char command[2000];
			strcpy(command,"openssl md5 ");strcat(command,reg);strcat(command," | cut -d\" \" -f2 > md5");system(command);strcpy(command,"date -r ./");strcat(command,reg);strcat(command," +%Y%m%d%H%M%S > date");system(command);strcpy(command,"paste md5 date > temp");
			system(command);
			strcpy(command,"rm md5 date");
			system(command);
			FILE*f=fopen("temp","r");
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				sendto(server,stream,1024,0,(struct sockaddr*)&cadd,len);
			}
			system("rm temp");
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			sendto(server,stream,1024,0,(struct sockaddr*)&cadd,len);
			fclose(f);
		}else if(strcmp(te,"indexlong")==0)
		{
			system("find . -printf '%p %TY/%Tm/%Td %TH:%Tm:%Tm size:%k \n' > temp");
			FILE*f=fopen("temp","r");
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				sendto(server,stream,1024,0,(struct sockaddr*)&cadd,len);
			}
			system("rm temp");
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			sendto(server,stream,1024,0,(struct sockaddr*)&cadd,len);
			fclose(f);
		}else if(strcmp(te,"indexshort")==0)
		{
			strncpy(reg,(char*)data+strlen("indexshort "),100);
			printf("Timestamps are is %s\n",reg);
			char *myinput = strtok (reg," ");
			char lower[2000],upper[2000];
			strcpy(lower,myinput);
			myinput = strtok (NULL, " ");
			strcpy(upper,myinput);
			system("ls -l --time-style=+%Y%m%d%H%M%S -t > temp");
			FILE* f1 = fopen( "temp", "r" );
			FILE* f2 = fopen( "out", "w" );
			char buff[20000],cpy[20000];
			int cntr=0;
			while ( fgets( buff, 1000, f1 ) != NULL )
			{
				if(cntr!=0 && cntr!=1)
				{
					strcpy(cpy,buff);
					int place=0;
					myinput = strtok (buff," ");
					while (myinput != NULL)
					{
						if(place==5)
						{
							printf("%s\n", myinput);
							if(strcmp(myinput,lower)>0 && strcmp(myinput,upper)<0)
							{
								fprintf(f2,"%s",cpy);
							}
						}
						place++;
						myinput = strtok (NULL," ");
					}
				}
				cntr++;
			}
			fclose(f1);
			fclose(f2);
			FILE *f = fopen("out","r");
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				sendto(server,stream,1024,0,(struct sockaddr*)&cadd,sizeof(struct sockaddr));
			}
			system("rm temp");
			system("rm out");
			memset(stream,0,1024);
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			sendto(server,stream,1024,0,(struct sockaddr*)&cadd,sizeof(struct sockaddr));
			fclose(f);
		}else if(strcmp(te,"regex")==0)
		{
			strncpy(reg,(char*)data+strlen("regex "),100);
			char comm[2000];
			strcpy(comm,"find . -name \"");
			strcat(comm,reg);
			strcat(comm,"\" > temp");
			system(comm);
			FILE *f = fopen("temp","r");
			char stream[2000];
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server seding %s\n",stream);
				sendto(server,stream,1024,0,(struct sockaddr*)&cadd,sizeof(struct sockaddr));
			}
			system("rm temp");
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			sendto(server,stream,1024,0,(struct sockaddr*)&cadd,sizeof(struct sockaddr));
			fclose(f);
		}else if(strcmp(te,"upload")==0)
		{
			char a[100];
			strncpy(a,(char*)data+strlen("upload "),100);
			printf("File: %s\nUpload incoming... Type \"Allow\" or \"Reject\"\n",a);
			close(fd[0]);
			write(fd[1],a,strlen(a)+1);
		}else if(strcmp(te,"download")==0)
		{
			char filename[200];
			strcpy(filename,data+strlen("download "));
			char stream[2000];
			FILE*f=fopen(filename,"rb");
			if(!f)
			{
				printf("error opening file\n");
				continue;
			}
			printf("starting upload of %s\n",filename);
			while(!feof(f))
			{
				int b=fread(stream,1,1024,f);
				stream[b]='\0';
				printf("server sending %s\n",stream);
				b=sendto(server,stream,1024,0,(struct sockaddr*)&cadd,sizeof(struct sockaddr));
			}
			char end[]= "End Of File";
			strcpy(stream,end);
			printf("server seding %s\n",stream);
			sendto(server,stream,1024,0,(struct sockaddr*)&cadd,sizeof(struct sockaddr));
			fclose(f);
			char packet[1024];
			char cmd[1000],temp[100];
			int stlen,ii;
			strcpy(cmd,"md5sum ");
			strcat(cmd,filename);
			strcat(cmd," | awk '{print $2, $1}'> t1");
			system(cmd);
			strcpy(cmd,"stat -c%s ");
			strcat(cmd,filename);
			strcat(cmd," > t2");
			system(cmd);
			strcpy(cmd,"paste t1 t2 | awk '{print $1, $2, $3}' > md5");
			system(cmd);
			strcpy(cmd,"rm t1 t2");
			system(cmd);

			FILE *f1= fopen("md5","r");
			fgets( packet, 100, f1 );
			stlen=strlen(packet);
			packet[stlen-1]='\0';
			printf("length of packet now is %d\n",(int)strlen(packet));
			printf("packet header : %s\n",packet);
			fclose(f1);
			strcpy(cmd,"rm md5");
			system(cmd);

			memset(stream,0,1024);
			strcpy(stream,packet);
			printf("server seding %s\n",stream);
			sendto(server,stream,1024,0,(struct sockaddr*)&cadd,sizeof(struct sockaddr));
		}else{
			printf("%s\n" , data);
			fflush(stdout);
		}
	}
	close(server);
}

void tcp_client( int c1){
	int newconnection, bytes_recieved;  
	char receiver_data [1024],recv_data[1024];
	char regex[100];
	char fileName[100];
	struct hostent *host;
	struct sockaddr_in server_addr;  
	const char* localhost="127.0.0.1";

	host = gethostbyname(localhost);

	if ((newconnection = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("Error in socket");
		fprintf(stderr,"Closing program");
		exit(1);
	}


	server_addr.sin_family = AF_INET;     
	server_addr.sin_port = htons(c1);   
	server_addr.sin_addr = *((struct in_addr *)host->h_addr);
	bzero(&(server_addr.sin_zero),8); 




	while (connect(newconnection, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) == -1) ;

	char *myinput;
	char copy[1024];
	while(1)
	{
		gets(receiver_data); 

shortcut :		
		strcpy(copy,receiver_data);
		myinput = strtok (copy," ");


		if(myinput!=NULL)
		{
			if(strcmp(myinput,"FileHash")==0) 
			{
				myinput = strtok (NULL, " ");
				if(myinput !=NULL)
				{
					if(strcmp(myinput,"Verify")==0)
					{
						myinput = strtok (NULL, " ");
						if(myinput!=NULL)
						{
							strcpy(receiver_data,"fileverify");
							strcat(receiver_data,myinput);

							send(newconnection, receiver_data,strlen(receiver_data), 0);
							while(1)
							{
								bytes_recieved=recv(newconnection,recv_data,1024,0);
								recv_data[bytes_recieved] = '\0';
								if(strcmp(recv_data,"End Of File") == 0)break;
								fwrite(recv_data,1,bytes_recieved,stdout);

							}

							continue;
						}

					}
					else if(strcmp(myinput,"CheckAll")==0)
					{
						strcpy(receiver_data,"filecheck");
						send(newconnection, receiver_data,strlen(receiver_data), 0);
						while(1)
						{
							bytes_recieved=recv(newconnection,recv_data,1024,0);
							recv_data[bytes_recieved] = '\0';
							if(strcmp(recv_data,"End Of File") == 0)break;
							fwrite(recv_data,1,bytes_recieved,stdout);

						}

						continue;
					}
				}
				else printf("wrong usage of FileHash\n");
			}
			else if(strcmp(myinput,"IndexGet")==0)	
			{
				printf ("%s\n",myinput);
				myinput = strtok (NULL, " ");
				if(myinput!=NULL)
				{

					if(strcmp(myinput,"ShortList")==0)
					{
						myinput = strtok (NULL, " ");
						if(myinput!=NULL)
						{
							strcpy(receiver_data,"indexshort");
							strcat(receiver_data,myinput);
							myinput = strtok (NULL, " ");
							if(myinput!=NULL)
							{
								strcat(receiver_data," ");
								strcat(receiver_data,myinput);

								send(newconnection, receiver_data,strlen(receiver_data), 0);


								while(1)
								{
									bytes_recieved=recv(newconnection,recv_data,1024,0);
									recv_data[bytes_recieved] = '\0';
									if(strcmp(recv_data,"End Of File") == 0)break;
									fwrite(recv_data,1,bytes_recieved,stdout);

								}

								continue;
							}
						}
						else printf("Wrong Usage. Please Type \"IndexGet ShortList <filename>\"	\n");	
					}

					if(strcmp(myinput,"RegEx")==0)
					{
						myinput = strtok (NULL, " ");
						if(myinput!=NULL)
						{
							strcpy(receiver_data,"regex");
							strcat(receiver_data,myinput);
							printf("receiver_data : %s\n",receiver_data);
							send(newconnection, receiver_data,strlen(receiver_data), 0);

							while(1)
							{
								bytes_recieved=recv(newconnection,recv_data,1024,0);
								recv_data[bytes_recieved] = '\0';
								if(strcmp(recv_data,"End Of File") == 0)break;
								fwrite(recv_data,1,bytes_recieved,stdout);		
							}
							continue;
						}
						else
						{
							printf("Regular Expression not Given, please try again\n");
						}
					}


					else if(strcmp(myinput,"LongList")==0)
					{
						strcpy(receiver_data,"indexlong");
						send(newconnection, receiver_data,strlen(receiver_data), 0); 

						while(1)
						{

							bytes_recieved=recv(newconnection,recv_data,1024,0);
							recv_data[bytes_recieved] = '\0';
							if(strcmp(recv_data,"End Of File") == 0)break;
							fwrite(recv_data,1,bytes_recieved,stdout);

						}

						continue;
					}
				}
			}

			else if(strcmp(myinput,"Upload") == 0)
			{ 

				myinput = strtok (NULL, " ");

				char arr[100];
				strcpy(arr,myinput);
				printf("Waiting for permission to Upload %s\n",arr);	
				strcpy(receiver_data,"U ");
				strcat(receiver_data,myinput);
				write(newconnection,receiver_data,1024);

			}
			else if(strcmp(myinput,"FileDownload") == 0)
			{
				myinput = strtok (NULL, " ");
				if(myinput!=NULL)
				{
resend:			strcpy(receiver_data,"download ");
			strcat(receiver_data,myinput);
			write(newconnection,receiver_data,1024);


			FILE *fp1 = fopen(myinput,"w");
			strcat(myinput,"(1)");
			memset(recv_data,0,1024);
			while(1){
				bytes_recieved=read(newconnection,recv_data,1024);

				if(strcmp(recv_data,"End Of File")==0)
				{
					break;
				}	
				fwrite(recv_data, 1,bytes_recieved, fp1);

			}

			fclose(fp1);

			bytes_recieved=read(newconnection,recv_data,1024);

			char dt[1024];
			strcpy(dt,recv_data);
			char * hsh = strtok(dt," ");

			if(hsh == NULL) 
				hsh = strtok(NULL," ");
			strcat(hsh,"(1)");

			if(strcmp(hsh,myinput) == 0){
				hsh = strtok(NULL," ");
				char fname[100];
				strcpy(fname,myinput);
				char packet[1024];
				char cmd[1000],temp[100];
				int stlen,ii;


				strcpy(cmd,"md5sum ");
				strcat(cmd,fname);
				strcat(cmd," | awk '{print $1}'> t1");
				system(cmd);
				FILE *f1= fopen("t1","r");
				fgets( packet, 100, f1 );
				stlen=strlen(packet);
				packet[stlen-1]='\0';
				fclose(f1);
				strcpy(cmd,"rm t1");
				system(cmd);


				if(strcmp(packet,hsh) == 0 ) printf(" TRANSFER COMPLETE\n");
				else {
					printf("Error in md5. Will resend\n") ;
					goto resend;
				}		
			}
			else printf("File names dont match\n");
				}

				else printf("Wrong use of download");

			}
			else if(strcmp(myinput,"Allow") == 0)
			{	

				close(fd[1]);
				int nbytes = read(fd[0],fileName,sizeof(fileName));
				strcpy(receiver_data,"Download ");
				strcat(receiver_data,fileName);
				goto shortcut;
			}
			else if(strcmp(myinput,"Reject") == 0)
			{

				printf("Process stopped\n");
			}
			else{

				if ((strcmp(receiver_data , "quit") || quit ==1))
				{

					send(newconnection, receiver_data,strlen(receiver_data), 0); 
					fflush(stdout);
					close(newconnection);
					kill(childPid, SIGTERM);
					exit(0);
					break;
				}

				else
					send(newconnection, receiver_data,strlen(receiver_data), 0);  
			}

		}


	}
	return;
}



int main()
{
	int port,choice=0;
	printf("enter port number: ");
	scanf("%d",&port);
	printf("0 : tcp || 1 : udp\n");
	scanf("%d",&choice);
	quit = 0;
	pipe(fd);
	int pid = fork();
	if(pid)
	{
		int ch;
		printf("Connect to: ");
		scanf("%d",&ch);
		if(choice==0)
			tcp_client(ch);
		else
			udp_client(ch);
	}
	else {
		//server
		if(choice==0)
			tserver(port);
		else
			userver(port);
	}
	return 0;
}
